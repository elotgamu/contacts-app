import { type FormEvent, useEffect, useState } from "react";

import {
  Form,
  Links,
  LiveReload,
  Meta,
  NavLink,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
  useNavigation,
  useSubmit,
} from "@remix-run/react";

import { json, redirect } from "@remix-run/node";

// custom api methods
import { createEmptyContact, getContacts } from "./data";

// types
import { type ActionFunctionArgs, type LinksFunction } from "@remix-run/node";

// import the styles
import appStylesHref from "./app.css";

// prop for the nav link class
type NavLinkClassProps = {
  isActive: boolean;
  isPending: boolean;
};
// array of resources
export const links: LinksFunction = () => [
  { rel: "stylesheet", href: appStylesHref },
];

/**
 *
 * Data loader to fetch contacts data into the
 * UI component. This serve as connector
 * between the handler and the UI
 *
 * If request contains a search
 * we filter the whole set of contact to
 * a narrow list that match the criteria,
 * otherwise we return the whole set of contacts
 *
 */
export const loader = async ({ request }: ActionFunctionArgs) => {
  const url = new URL(request.url);
  const filter = url.searchParams.get("q");
  const contacts = await getContacts(filter);
  return json({ contacts, searchCriteria: filter });
};

/**
 *
 * Action for the `New Contact`
 * This is the handler for the
 * POST request to the root route
 * We redirect the user to the edit view
 */
export const action = async () => {
  const contact = await createEmptyContact();
  console.log("created empty contact ", contact);
  return redirect(`/contacts/${contact.id}/edit`);
};

export default function App() {
  const [query, setQuery] = useState<string>("");

  const submit = useSubmit();
  const navigation = useNavigation();

  const { contacts, searchCriteria } = useLoaderData<typeof loader>();

  useEffect(() => {
    // const searchField = document.getElementById("q");
    // if (searchField instanceof HTMLInputElement) {
    //   searchField.value = searchCriteria || "";
    // }

    setQuery(searchCriteria || "");
  }, [searchCriteria]);

  function getNavLinkClassName({ isActive, isPending }: NavLinkClassProps) {
    if (isActive) return "active";
    if (isPending) return "pending";
    return "";
  }

  // derive the searching state
  // we consider user is searching as far a
  // query param is provided
  // once the result is provided q is no longer available
  const isSearching =
    navigation.location &&
    new URLSearchParams(navigation.location.search).has("q");

  // derive the class name for the detail section
  // we do not consider the isSearching to avoid
  // fading the main screen
  const detailSectionClassName =
    navigation.state === "loading" && !isSearching ? "loading" : "";

  // on change handler for the form event
  // we replace the the navigation stack
  // to prevent populating the history with unneeded
  // individual keystrokes
  function onFormChange(event: FormEvent<HTMLFormElement>) {
    const isFirstSearch = searchCriteria === null;
    submit(event.currentTarget, { replace: !isFirstSearch });
  }

  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body>
        <div id="sidebar">
          <h1>Remix Contacts</h1>
          <div>
            <Form id="search-form" role="search" onChange={onFormChange}>
              <input
                id="q"
                className={isSearching ? "loading" : ""}
                aria-label="Search contacts"
                placeholder="Search"
                type="search"
                name="q"
                value={query}
                onChange={(e) => setQuery(e.currentTarget.value)}
              />
              <div id="search-spinner" aria-hidden hidden={!isSearching} />
            </Form>
            <Form method="post">
              <button type="submit">New</button>
            </Form>
          </div>
          <nav>
            {contacts.length ? (
              <ul>
                {contacts.map((contact) => (
                  <li key={contact.id}>
                    <NavLink
                      className={getNavLinkClassName}
                      to={`contacts/${contact.id}`}
                    >
                      {contact.first || contact.last ? (
                        <>
                          {contact.first} {contact.last}
                        </>
                      ) : (
                        <i>No Name</i>
                      )}{" "}
                      {contact.favorite ? <span>★</span> : null}
                    </NavLink>
                  </li>
                ))}
              </ul>
            ) : (
              <p>
                <i>No contacts</i>
              </p>
            )}
          </nav>
        </div>

        <div id="detail" className={detailSectionClassName}>
          <Outlet />
        </div>

        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
