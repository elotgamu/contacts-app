import { type FormEvent, type FunctionComponent } from "react";
import { updateContact, type ContactRecord } from "../data";

import invariant from "tiny-invariant";

import {
  json,
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
} from "@remix-run/node";
import { useFetcher, Form, useLoaderData } from "@remix-run/react";

import { getContact } from "../data";

export const loader = async ({ params }: LoaderFunctionArgs) => {
  invariant(params.contactId, "Missing contactId param");
  const contact = await getContact(params.contactId);
  if (!contact) {
    throw new Response("Not Found", { status: 404 });
  }
  return json({ contact });
};

/**
 *
 * Handler to mark a contact as
 * favorite.
 */
export const action = async ({ params, request }: ActionFunctionArgs) => {
  invariant(params.contactId, "Missing contactId param");
  const formData = await request.formData();
  return updateContact(params.contactId, {
    favorite: formData.get("favorite") === "true",
  });
};

export default function Contact() {
  const { contact } = useLoaderData<typeof loader>();

  function handleDelete(event: FormEvent<HTMLFormElement>) {
    const response = confirm("Please confirm you want to delete this record.");

    if (!response) {
      event.preventDefault();
    }
  }

  return (
    <div id="contact">
      <div>
        <img
          alt={`${contact.first} ${contact.last} avatar`}
          key={contact.avatar}
          src={contact.avatar}
        />
      </div>

      <div>
        <h1>
          {contact.first || contact.last ? (
            <>
              {contact.first} {contact.last}
            </>
          ) : (
            <i>No Name</i>
          )}
          <Favorite contact={contact} />
        </h1>

        {contact.twitter ? (
          <p>
            <a href={`https://twitter.com/${contact.twitter}`}>
              {contact.twitter}
            </a>
          </p>
        ) : null}

        {contact.notes ? <p>{contact.notes}</p> : null}

        {/* <!-- action sections --> */}
        <div>
          <Form action="edit">
            <button type="submit">Edit</button>
          </Form>

          <Form action="destroy" method="post" onSubmit={handleDelete}>
            <button type="submit">Delete</button>
          </Form>
        </div>

        {/* <!-- end action sections --> */}
      </div>
    </div>
  );
}

const Favorite: FunctionComponent<{
  contact: Pick<ContactRecord, "favorite">;
}> = ({ contact }) => {
  const { Form, formData } = useFetcher();
  const favorite = formData
    ? formData.get("favorite") === "true"
    : contact.favorite;

  return (
    <Form method="post">
      <button
        name="favorite"
        aria-label={favorite ? "Remove from favorites" : "Add to favorites"}
        value={favorite ? "false" : "true"}
      >
        {favorite ? "★" : "☆"}
      </button>
    </Form>
  );
};
