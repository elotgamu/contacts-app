import invariant from "tiny-invariant";

import {
  json,
  redirect,
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
} from "@remix-run/node";
import { Form, useLoaderData, useNavigate } from "@remix-run/react";

// handlers
import { getContact, updateContact } from "../data";

/**
 * Loader handler to get the contact data
 * Throw 404 if contact id not found
 * otherwise return the contact data
 */
export const loader = async ({ params }: LoaderFunctionArgs) => {
  invariant(params.contactId, "Missing contact Id param");

  const contact = await getContact(params.contactId);
  if (!contact) {
    throw new Response("Not Found", { status: 404 });
  }

  return json({ contact });
};

/**
 *
 * Action handler for the edit contact form
 * - Validate the param
 * - Get updated values from the form data
 * - Call the update handler
 * - If success redirect the user to the detail view
 *
 */
export const action = async ({ params, request }: ActionFunctionArgs) => {
  invariant(params.contactId, "Missing contactId param");
  const formData = await request.formData();
  const updates = Object.fromEntries(formData);
  await updateContact(params.contactId, updates);
  return redirect(`/contacts/${params.contactId}`);
};

/**
 * Edit Contact component
 *
 * This file name follow Remix conventions
 *
 * The path should be the same however
 * we do want to render it inside
 * the `/contact/{contactId}/edit
 * to follow clean url pattern
 *
 * So by appending a:
 * - `_` (underscore) we tell remix not to nest the route
 * - `.` (dot) we tell remix to create a /edit path
 *
 * This result in this component to be rendered on
 *  `/contact/{contactId}/edit
 *
 * The form submit event is handled by the
 * `action` function defined in this file
 * This binding is provided by Remix
 * and it receives the form data as part of
 * the `request` prop, and as far as we provide a
 * HTML compliant entries from a form we can extract
 * the properties from the form data.
 *
 * Besides the binding of the form submit to the action
 * method, everything else is provided by the web
 * platform
 */
export default function EditContact() {
  const navigate = useNavigate();
  const { contact } = useLoaderData<typeof loader>();

  return (
    <Form method="post" id="contact-form" key={contact.id}>
      <p>
        <span>Name</span>
        <input
          aria-label="First name"
          name="first"
          type="text"
          defaultValue={contact.first}
          placeholder="First"
        />
        <input
          aria-label="Last name"
          defaultValue={contact.last}
          name="last"
          placeholder="Last"
          type="text"
        />
      </p>

      <label>
        <span>Twitter</span>
        <input
          name="twitter"
          type="text"
          defaultValue={contact.twitter}
          placeholder="@jack"
        />
      </label>

      <label>
        <span>Avatar URL</span>
        <input
          name="avatar"
          type="text"
          aria-label="Avatar URL"
          defaultValue={contact.avatar}
          placeholder="https://example.com/avatar.jpg"
        />
      </label>

      <label>
        <span>Notes</span>
        <textarea name="notes" defaultValue={contact.notes} rows={6} />
      </label>

      <p>
        <button type="submit">Save</button>
        <button type="button" onClick={() => navigate(-1)}>
          Cancel
        </button>
      </p>
    </Form>
  );
}
