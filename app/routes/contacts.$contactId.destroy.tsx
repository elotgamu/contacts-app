import invariant from "tiny-invariant";
import { redirect, type ActionFunctionArgs } from "@remix-run/node";

// handler to delete contact
import { deleteContact } from "../data";

/**
 *
 * Create an `action` to handler to
 * manage the destroy call to the
 * `/contact/{contactId}/destroy
 *
 * We do not need a UI component
 * only a handler however we need the
 * naming convention to remix
 * to understand that this will handle
 * a request at an specific path
 */
export const action = async ({ params }: ActionFunctionArgs) => {
  invariant(params.contactId, "Missing contactId param");
  await deleteContact(params.contactId);
  return redirect("/");
};
